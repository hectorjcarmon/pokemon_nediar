﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class GetDataFromApi : MonoBehaviour
{
    public static GetDataFromApi inst;

    private UIManager instancia;

    //public JsonDataClass jsonData;

    public void Awake()
    {
        if (inst == null)
        {
            inst = this;
            DontDestroyOnLoad(gameObject);
        }
        else
            Destroy(gameObject);
        
    }

    public void Start()
    {
        instancia = UIManager.inst;
    }
        

    public IEnumerator GetPokemonAtIndexList(string urlAPI)
    {

        UnityWebRequest pokeInfoRequest = UnityWebRequest.Get(urlAPI);

        yield return pokeInfoRequest.SendWebRequest();

        if (pokeInfoRequest.isHttpError || pokeInfoRequest.isNetworkError)
        {
            Debug.LogError(pokeInfoRequest.error);
            yield break;
        }
                
        instancia.ProcessJsonData(pokeInfoRequest.downloadHandler.text);
    }

    
    public IEnumerator GetInfoPokemon(string url)
    {
        UnityWebRequest pokeSingleInfoRequest = UnityWebRequest.Get(url);

        yield return pokeSingleInfoRequest.SendWebRequest();

        if (pokeSingleInfoRequest.isHttpError || pokeSingleInfoRequest.isNetworkError)
        {
            Debug.LogError(pokeSingleInfoRequest.error);
            
            yield break;
        }
        
        instancia.ProcessJsonDataSinglePoke(pokeSingleInfoRequest.downloadHandler.text);

        
    }

    public IEnumerator GetTexturePokemon(string url)
    {
        UnityWebRequest pokeImageRequest = UnityWebRequestTexture.GetTexture(url);

        yield return pokeImageRequest.SendWebRequest();

        if (pokeImageRequest.isNetworkError || pokeImageRequest.isHttpError)
        {
            yield break;
        }

        instancia.image.texture = DownloadHandlerTexture.GetContent(pokeImageRequest);
    }

    public void StarRoutine(string url)
    {        
        StartCoroutine(GetPokemonAtIndexList(url));               
    }

    public void StarRoutineSinglePoke(string url)
    {
        StartCoroutine(GetInfoPokemon(url));
    }

    public void StarRoutineGetTexture(string url)
    {
        StartCoroutine(GetTexturePokemon(url));
    }
    
}
