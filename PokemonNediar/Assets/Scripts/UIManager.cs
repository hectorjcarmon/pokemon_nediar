﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIManager : MonoBehaviour
{
    public static UIManager inst;

    //pokedex
    public List<Button> pokeButtons;
    public GameObject panelPokedex;
    public GameObject panelGame;
    public GameObject panelInfo;
    public Text pokemonName;
    public Text pokemonWeight;
    public Text pokemonHeight;
    public InputField searchInput;
    public RawImage image;

    //ingame
    public Text catchText;

    public JsonDataClass jsonData;
    public JsonInfoPoke jsoninfoPoke;
    public bool error = false;

    private string baseURL = "https://pokeapi.co/api/v2/pokemon/?offset=0&limit=10";

    public void Awake()
    {
        if (inst == null)
        {
            inst = this;
            DontDestroyOnLoad(gameObject);
        }
        else
            Destroy(gameObject);
    }

    public void Start()
    {
        panelPokedex.SetActive(false);
    }

    public void ProcessJsonData(string downloadHandler)
    {
        jsonData = JsonUtility.FromJson<JsonDataClass>(downloadHandler);
        
        for (int i = 0; i < jsonData.results.Count; i++)
        {
            string jdat = jsonData.results[i].url;
            pokeButtons[i].GetComponentInChildren<Text>().text = jsonData.results[i].name;
            pokeButtons[i].onClick.RemoveAllListeners();
            pokeButtons[i].onClick.AddListener(delegate { OnPokeSingleInfo(jdat); });
        }
    }

    
    public void ProcessJsonDataSinglePoke(string downloadHandler)
    {
        jsoninfoPoke = JsonUtility.FromJson<JsonInfoPoke>(downloadHandler);
        
        
        pokemonName.text = jsoninfoPoke.name;
        pokemonWeight.text = jsoninfoPoke.weight.ToString();
        pokemonHeight.text = jsoninfoPoke.height.ToString();
        GetDataFromApi.inst.StarRoutineGetTexture(jsoninfoPoke.sprites.front_default);
        panelInfo.SetActive(true);
    }

    

    public void OnStartPokedex()
    {
        panelPokedex.SetActive(true);
        panelGame.SetActive(false);
        GetDataFromApi.inst.StarRoutine(baseURL);
    }

    public void OnNextListPokedex()
    {
        GetDataFromApi.inst.StarRoutine(jsonData.next);
    }

    public void OnPreviousListPokedex()
    {
        if(jsonData.previous !="")
        GetDataFromApi.inst.StarRoutine(jsonData.previous);
    }

    public void OnPokeSingleInfo(string urlpke)
    {
        panelInfo.SetActive(true);
        
        GetDataFromApi.inst.StarRoutineSinglePoke(urlpke);
        
    }

    public void OnSearch()
    {
        string singleUrl = "https://pokeapi.co/api/v2/pokemon/" + searchInput.text;        
        GetDataFromApi.inst.StarRoutineSinglePoke(singleUrl);        
    }

    public void OnResetTxt()
    {
        pokemonName.text = "";
        pokemonHeight.text = "";
        pokemonWeight.text = "";
        panelInfo.SetActive(false);
    }

    public IEnumerator CachtTexts(string str)
    {
        catchText.text = str;
        yield return new WaitForSeconds(3);
        catchText.text = "";
    }

}
