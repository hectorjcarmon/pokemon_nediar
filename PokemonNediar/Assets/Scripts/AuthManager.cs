﻿using UnityEngine;
using UnityEngine.UI;
using Firebase.Auth;

public class AuthManager : MonoBehaviour
{
    public InputField inputEmail;
    public InputField inputPass;
    public Text textInfo;

    public GameObject panelLogueo;

    private string displayName;
    private bool signedIn;

    protected FirebaseAuth auth;
    protected FirebaseUser user;

    private void Start()
    {
        //LogOut();
        InitializeFirebase();
    }

    private void Update()
    {
        //auth.SignOut();
        if (signedIn)
        {
            panelLogueo.SetActive(false);
        }
    }


    void InitializeFirebase()
    {
        auth = Firebase.Auth.FirebaseAuth.DefaultInstance;
        auth.StateChanged += AuthStateChanged;
        AuthStateChanged(this, null);
    }

    public void CreateUserByEmail()
    {
        //auth = Firebase.Auth.FirebaseAuth.DefaultInstance;
        
        var email = inputEmail.text;
        var password = inputPass.text;

        auth.CreateUserWithEmailAndPasswordAsync(email, password).ContinueWith(task => {
            if (task.IsCanceled)
            {               
                Messages("ha ocurrido un error");
                return;
            }
            if (task.IsFaulted)
            {                
                Messages("ha ocurrido un error");
                return;
            }
                        
            FirebaseUser newUser = task.Result;
            
            Messages("Usuario " + newUser.Email + " ha sido creado");

            inputEmail.text = "";
            inputPass.text = "";

        });
    }

    public void LoginByEmail()
    {
        var email = inputEmail.text;
        var password = inputPass.text;

        auth.SignInWithEmailAndPasswordAsync(email, password).ContinueWith(task => {
            if (task.IsCanceled)
            {
                Debug.LogError("SignInWithEmailAndPasswordAsync was canceled.");
                Messages("ha ocurrido un error");
                return;
            }
            if (task.IsFaulted)
            {
                Debug.LogError("SignInWithEmailAndPasswordAsync encountered an error: " + task.Exception);
                Messages("ha ocurrido un error");
                return;
            }

            FirebaseUser newUser = task.Result;
            Debug.LogFormat("User signed in successfully: {0} ({1})",
                newUser.DisplayName, newUser.UserId);
            Debug.Log(newUser.DisplayName + " " + newUser.UserId + " esta es la info del logueo");

            //Messages("Usuario " + newUser.Email + " ha sido encontrado");
            
        });
    }

    public void Messages(string msg)
    {
        textInfo.text = msg;
        
    }

    void AuthStateChanged(object sender, System.EventArgs eventArgs)
    {
        if (auth.CurrentUser != user)
        {
            signedIn = user != auth.CurrentUser && auth.CurrentUser != null;
            if (!signedIn && user != null)
            {
                Debug.Log("Signed out " + user.UserId);
            }
            user = auth.CurrentUser;
            if (signedIn)
            {
                Debug.Log("Signed in " + user.UserId);
                displayName = user.DisplayName ?? "";
                //emailAddress = user.Email ?? "";                
            }
        }
    }

    public void LogOut()
    {        
        signedIn = false;
        panelLogueo.SetActive(true);
        inputEmail.text = "";
        inputPass.text = "";
        auth.SignOut();
    }

    void OnDestroy()
    {
        auth.StateChanged -= AuthStateChanged;
        auth = null;
        Debug.Log("se activo el Ondestroy");
    }

}
