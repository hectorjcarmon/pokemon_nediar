﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PokemonScript : MonoBehaviour
{
    public bool iscaught = false;
    
    public void ActivePokemon()
    {
        gameObject.SetActive(true);
        
    }

    public void DeactivePokemon()
    {
        gameObject.SetActive(false);
        
    }

    private void OnCollisionEnter(Collision collision)
    {
        int randomCatch = Random.Range(0, 10);        
        //int randomCatch = 4;
        if (randomCatch >=5)
        {
            //UIManager.inst.catchText.text = "Atrapado";
            StartCoroutine(UIManager.inst.CachtTexts("Atrapado"));
            iscaught = true;
            DeactivePokemon();            
        }
        else
            StartCoroutine(UIManager.inst.CachtTexts("NO! Atrapado"));
    }

}
