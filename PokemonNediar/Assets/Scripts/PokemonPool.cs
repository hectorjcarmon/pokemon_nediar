﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PokemonPool : MonoBehaviour
{

    public static PokemonPool instance;
    private PokemonScript[] pokemons;

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
            DontDestroyOnLoad(gameObject);
        }
        else
            Destroy(gameObject);

    }
    void Start()
    {
        pokemons = GetComponentsInChildren<PokemonScript>();
        foreach (var item in pokemons)
        {
            item.DeactivePokemon();
        }
    }

    public void DeactiveItems()
    {
        //pokemons = GetComponentsInChildren<PokemonScript>();
        foreach (var item in pokemons)
        {
            item.DeactivePokemon();
        }
    }

    

    public PokemonScript GetPokemon()
    {       
        int temp = Random.Range(0,pokemons.Length);

        while (pokemons[temp].iscaught)
        {
            temp = Random.Range(0, pokemons.Length);
        }

        return pokemons[temp];
    }

    
}
