﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PokeballSwipe : MonoBehaviour
{
    private Vector2 starPos;
    private Vector2 endPos;
    private Vector2 direction;

    [SerializeField]
    private Vector3 starposition;

    private float touchStart;
    private float touchFinish;
    private float interval;

    [SerializeField]
    private float throwForceXY;
    [SerializeField]
    private float throwForceZ;

    Rigidbody rb;

    void Start()
    {
        rb = GetComponent<Rigidbody>();
        starposition = transform.position;
    }


    void Update()
    {
        if (Input.touchCount > 0 && Input.GetTouch(0).phase == TouchPhase.Began)
        {
            touchStart = Time.time;
            starPos = Input.GetTouch(0).position;
        }

        if (Input.touchCount > 0 && Input.GetTouch(0).phase == TouchPhase.Ended)
        {
            touchFinish = Time.time;
            interval = touchFinish - touchStart;

            endPos = Input.GetTouch(0).position;

            direction = starPos - endPos;

            rb.isKinematic = false;
            rb.AddForce(-direction.x * throwForceXY, -direction.y * throwForceXY, throwForceZ / interval);

            Invoke("BallActiveTime", 4);
        }

        
    }

    private void StartPosition()
    {
        if(transform.position.y < 0.3f)
        {
            transform.position = starposition;
        }
    }

    private void BallActiveTime()
    {
        rb.isKinematic = true;
        transform.position = starposition;
    }
    
}
