﻿using System.Collections.Generic;
using System;

[Serializable]
public class JsonDataClass
{
    public string next;
    public string previous;
    public List<Results> results;
}

[Serializable]
public class Results
{
    public string name;
    public string url;
}

[Serializable]
public class JsonInfoPoke
{
    public string name;
    public int weight;
    public int height;
    public Sprites sprites;
    
}

[Serializable]
public class Sprites
{
    public string front_default;
}


